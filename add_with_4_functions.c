//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
float input()
{
    float number;
	printf("Enter number:\n");
	scanf("%f", &number);
	return number;
}
float addition(float num1, float num2)
{
    return num1+num2;
}
void display(float n1,float n2, float sum)
{
    printf("Sum of %f + %f is: \n%f", n1,n2,sum);
}
int main()
{
    float number1,number2,result;
	number1=input();
	number2=input();
	result=addition(number1,number2);
	display(number1,number2,result);
	return 0;
}