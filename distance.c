#include<stdio.h>
#include<math.h>
float insert(char a,int b)
{
  float n;
  printf("Enter the coordinate %c%d:\n",a,b);
  scanf("%f",&n);
  return n;
}

float dist(float x1,float y1,float x2,float y2)
{
  float dist;
  dist=((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
  return dist;
}

void display(float x1,float y1,float x2,float y2,float d)
{
  printf("The distance between the 2 points %f,%f and %f,%f is: \n%f",x1,y1,x2,y2,sqrt(d));
}

int main()
{
  float x1,y1,x2,y2,d;
  x1=insert('x',1);
  y1=insert('y',1);
  x2=insert('x',2);
  y2=insert('y',2);
  d=dist(x1,y1,x2,y2);
  display(x1,y1,x2,y2,d);
  return 0;
}