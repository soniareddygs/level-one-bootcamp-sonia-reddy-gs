//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction
{
    int n;
    int d;
};

typedef struct fraction frac;

frac input()
{
    frac f;
    printf("Enter the numerator:\n");
    scanf("%d",&f.n);
    printf("Enter the denominator:\n");
    scanf("%d",&f.d);
    return f;
}

int compute_gcd(int a, int b)
{
 int gcd;
 for(int i=1;i<=a && i<=b;i++)
 {
  if(a%i==0 && b%i==0)
  {
   gcd=i;
  }
 }
 return gcd;
}
frac compute(frac f1, frac f2)
{
    int gcd, i, num, den;
    num=(f1.n*f2.d)+(f1.d*f2.n);
    den=f1.d*f2.d;
    gcd=compute_gcd(num,den);
    frac sum;
    sum.n=num/gcd;
    sum.d=den/gcd;
    return sum;
}
void output(frac sum)
{
    printf("The added fraction is %d/%d",sum.n,sum.d);
}
int main()
{
    frac f1,f2,sum;
    f1=input();   
    f2=input();
    sum=compute(f1,f2);
    output(sum);
    return 0;
}

